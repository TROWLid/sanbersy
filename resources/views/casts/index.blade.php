@extends('layouts.master')
@section('content')
<div class="mx-2 mt-2">
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Cast Table</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      @if (session('Success'))
          <div class="alert alert-success">
              {{ session('Success') }}
          </div>
      @endif
      <a class="btn btn-primary" href="/cast/create">Create New Cast</a>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th style="width: 10px">#</th>
            <th style="width: 30%">Nama</th>
            <th style="width: 10px">Umur</th>
            <th>Bio</th>
            <th style="width: 10px">Action</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($casts as $key => $cast)
              <tr>
                  <td> {{ $key +1 }} </td>
                  <td> {{ $cast->nama }} </td>
                  <td> {{ $cast->umur }} </td>
                  <td> {{ $cast->bio }} </td>
                  <td style="display: flex">
                      <a href="/cast/{{$cast->id}}" class="btn btn-success btn-sm">show</a>
                      <a href="/cast/{{$cast->id}}/edit" class="btn btn-default btn-sm">edit</a>
                      <form action="/cast/{{$cast->id}}" method="POST">
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">@csrf @method('DELETE')
                      </form>
                  </td>
              </tr>
          @empty
              <tr>
                  <td colspan="4" align="center"> No Data </td>
              </tr>
          @endforelse
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
</div>
@endsection